require.config({
//    urlArgs: "bust=" +  (new Date()).getTime(),
    baseUrl: "lib",
    paths: { "d3": "d3.v3" },
    shim: { "d3" : { exports: "d3" },
            "svg" : { exports: "SVG" },
            "svg.export": [ "svg" ]
          }
});

require(["trademapper"], function(tm) {
    tm.init(document.getElementById("mapcanvas"));
});

// XXX DEBUG

require(["trademapper", "trademapper.csv", "window_loaded", "trademapper.export"], function(tm) {
    var cb = function (r) {
        tm.routes = r;
        tm.update();
    };

    function handleDelete() {
        if (confirm("Delete stack point?")) {
            tm.del_stack(this.stackpoint);
            d3.select(this).remove();
            tm.update();
        }
    }

//    tm.csv.loadRouteCSV(cb);
    d3.select("#load_csv_button").on("click", function() { tm.csv.loadRouteCSV(cb); });
    d3.select("#random_button").on("click", function() { tm.routes = tm.random_routes(20); tm.update(); });
    //tm.routes = [ new tm.Route("US", "ID", 10), new tm.Route("ID", "AU") ];
    tm.routes = [ new tm.Route("US", "Australia", 10), new tm.Route("Brazil", "China", 5) ];
                    //new tm.Route("Canada", "Spain", 4), new tm.Route("Canada", "South Africa", 1),
                    //new tm.Route("Sweden", "Mexico", 5)];
    tm.update();

    /* handle creating of stack points in the ui */
    var drag = d3.behavior.drag()
        .on("drag", function () {
            d3.event.sourceEvent.stopPropagation();
            d3.select(this).attr("cx", d3.event.x)
                .attr("cy", d3.event.y);
        })
        .on("dragend", function () {
            var e = d3.select(this);
            d3.event.sourceEvent.stopPropagation();
            e.classed("dragged", false);
            e.property("stackpoint")
                .point({
                    x: Number(e.attr("cx")),
                    y: Number(e.attr("cy"))
                });
            tm.update();
        })
        .on("dragstart", function () {
            d3.event.sourceEvent.stopPropagation();
            d3.select(this).classed("dragged", true);
        });
     
    d3.select("#mapcanvas")
        .on("click", function () {
            console.log("click");
            var loc = d3.mouse(this);
            // default width of 20
            tm.add_stack({x: loc[0], y: loc[1]}, 20);
            tm.update();
            d3.selectAll("circle.stackpoint")
                // cancel the add-new-circle function
                .on("click", function () {
                    d3.event.stopPropagation();
                }).on("contextmenu", function () {
                    // don't show the built-in context menu
                    d3.event.preventDefault();
                    handleDelete.call(this);
                }).call(drag);
        }, false);

    tm.exporter.init_ui(document.getElementById("export_form"),
                        document.getElementById("mapcanvas"));


});

require(["trademapper.csv", "window_loaded"], function (tmcsv) {

});
