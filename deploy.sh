#!/bin/bash

HOST=stelo.org.uk
USER=steloorg
DIR=www/trademapper

EXCLUDES_FILE=.deploy.exclude

TAGNAME=deploy

BATCH_FILE=`mktemp`

#ARCHIVE_FILE=$HOME/Dropbox/Paul/trademapper.zip
ARCHIVE_FILE=$HOME/todelete/trademapper.zip

# empty the file
cat </dev/null >$BATCH_FILE

function action() {

    cmd=$1; shift;
    (for n in $@
    do
    case "${cmd}" in
        "put")
            echo "put" \"$n\" \"$n\"
            ;;
        "nput")
            echo "put" \"$n\"
            ;;
        *)
            echo "$cmd" \"$n\"
    esac
    done) >>$BATCH_FILE
}

trap "rm -v ${BATCH_FILE}" EXIT

# find out which files have changed since last deploy
for f in 
do
    echo $f
done

# filter out files that are in the excludes in the file
function exclude() {
    for f in $@
    do
        git check-attr export-ignore -- ${f} | grep 'export-ignore: set' >/dev/null
        if [[ "$?" -ne 0 ]]
        then
            echo $f
        fi
    done
}

# create the archive file
git archive --format=zip --output=${ARCHIVE_FILE} HEAD

action cd ${DIR}
action put $(exclude $(git diff-index --name-only $TAGNAME --))
action nput ${ARCHIVE_FILE}

sftp -b "${BATCH_FILE}" $USER@$HOST

exit
if [[ "$?" == "0" ]]
then
    DATE_TAG=${TAGNAME}_$(date '+%d%b%Y')

    echo "Tagging HEAD ${TAGNAME} and ${DATE_TAG}"
    git tag -f ${TAGNAME}
    git tag -f ${DATE_TAG}
else
    echo "failed, not tagging"
fi
