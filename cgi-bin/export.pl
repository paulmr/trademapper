#!/usr/bin/perl
use strict;
use CGI;
use File::Temp;

my $q = CGI->new;

my $svg_data = $q->param('svg_data');

my $filetype;
if($q->param('filetype')) {
    $filetype = $q->param('filetype');
} else {
    $filetype = "png";
}

my $fname = mktemp("trademapperXXXXX");

print $q->header(-type=>"image/$filetype",
                 -Content_Disposition=>"attachment; filename=trademapper.$filetype");

if($filetype ne "svg") {
    open(CONVERTER, "|convert svg:- $filetype:$fname");
    print CONVERTER $svg_data;
    close(CONVERTER);

    open(CONVERTED, "<$fname");
    while(<CONVERTED>) {
        print;
    }
    close(CONVERTED);
    unlink($fname);
} else {
    print $svg_data;
}

