define(["trademapper", "d3"], function(tm, d3) {

    return tm.exporter = {
        svg: function (node, width, height) {

            // create the SVG XML document fragement, into which we
            // will build the svg docment to save.
            var ns = "http://www.w3.org/2000/svg";
            var doc = document.implementation.createDocument(ns, "svg", null);

            if(arguments.length < 4) {
                width = "800";
                height = "600";
            }

            // load the styling ready to add it to the svg node
            var xhr = new XMLHttpRequest();

            xhr.open("GET", "trademapper.svg.css", false);
            xhr.send();

            var style = doc.createElementNS(ns, "style");
            style.setAttribute("type", "text/css");
            style.appendChild(doc.createCDATASection(xhr.response));

            node = node.cloneNode(true);
            node.setAttribute("height", height);
            node.setAttribute("width", width);
            node.insertBefore(style, node.firstChild);

            doc.documentElement.appendChild(node);

            return (new XMLSerializer()).serializeToString(doc);
        },

        init_ui: function (frm_node, mapnode) {
            frm_node.onsubmit = function () {
                // export the image as svg and include it in the form submission
                var svg = tm.exporter.svg(mapnode);
                frm_node.getElementsByClassName("svg_data")[0].value = svg;
                return true;
            };
        }

    }
});
