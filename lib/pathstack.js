// vim:ft=javascript.d3js
"use strict";

define(["d3"], function (d3) {

    function PathStack(point) {
        this.paths = [];
        this.config = { width: 0.1, point: point, margin: 0, color: null };
    }

    PathStack.prototype = {
        add: function (path, seg, size) {
            this.paths.push({path: path, size: size, seg: seg});
            return this;
        },

        empty: function () {
            this.paths = []
        },

        dostack: function () {
            var i, totalSize = 0, step, startx, y, point = this.point(),
                paths = this.paths;

            if (paths.length < 1) {
                return this;
            }

            for(i = 0; i < paths.length; i++) {
                totalSize += paths[i].size + this.config.margin;
            }

            step = totalSize / paths.length;
            y    = point.y - (totalSize / 2);

            for (i = 0; i < paths.length; i++, y += step) {
                flattenSegment(paths[i].path, {
                    seg: paths[i].seg,
                    point:  { x: point.x, y: y },
                    width:  this.width()
                });
                if (this.config.color) {
                    d3.select(paths[i].path).style("stroke", this.config.color(i));
                }
            }
            return this;
        },

        width: function (n) {
            if (arguments.length < 1) {
                return this.config.width;
            } else {
                this.config.width = n;
                return this;
            }
        },

        point: function (n) {
            if (arguments.length < 1) {
                return this.config.point;
            } else {
                this.config.point = n;
                return this;
            }
        },

        margin: function (n) {
            if (arguments.length < 1) {
                return this.config.margin;
            } else {
                this.config.margin = n;
                return this;
            }
        },

        color: function (f) {
            if (arguments.length < 1) {
                return this.config.color;
            } else {
                this.config.color = f;
                return this;
            }
        },
    }
    // a and b are points, returns the distance between them
    function distance(svg, a, b) {
        var msr = svg.append("path")
            .attr("class", "measuring")
            .attr("d", "M " + a.x + "," + a.y +
                    " L " + b.x + "," + b.y),
            len = msr.node().getTotalLength();
        msr.remove();
        return len;
    }

    function flattenSegment(path, params) {
        var start, end, newSegs = [];

        if (!("seg" in params)) {
            params.seg = path.getPathSegAtLength(params.length);
        }

        if (!("width" in params)) {
            params.width = 0;
        }

        if (!("height" in params)) {
            params.height = 0;
        }

        start = path.coords[params.seg];
        end   = path.coords[params.seg + 1];

        path.coords.splice(params.seg + 1, 0,
            [Math.max(start[0], params.point.x - (params.width == 0 ? 0 : params.width / 2)),
                params.point.y - (params.height == 0 ? 0 : params.height / 2)],
            [Math.min(end[0], params.point.x + (params.width == 0 ? 0 : params.width / 2)),
                params.point.y + (params.height == 0 ? 0 : params.height / 2)]);
    }

    function getPathIntersection(path1, path2) {
        var length = 0, point, tID, i;

        var path2Points = [];

        function isPointIntersection(p1, p2, pTarg) {
            return (
                    (Math.min(p1.x, p2.x) - (tolerance / 2) <= pTarg.x) &&
                    (Math.max(p1.x, p2.x) + (tolerance / 2) >= pTarg.x) &&
                    (Math.min(p1.y, p2.y) - (tolerance / 2) <= pTarg.y) &&
                    (Math.max(p1.y, p2.y) + (tolerance / 2) >= pTarg.y)
                );
        }

        for(length = 0; length < path2.getTotalLength(); length += tolerance) {
            point = path2.getPointAtLength(length);
            path2Points.push(point);
        }

        for(length = 0; length <= path1.getTotalLength(); length += (tolerance / 2)) {
            point = path1.getPointAtLength(length);

            for(i = 1; i < path2Points.length; i++) {
                if (isPointIntersection(path2Points[i], path2Points[i - 1], point)) {
                    return { lengths: [length, i * tolerance], point: point };
                }
            }
        };
        // no intersection found
        return null;
    }

    // search the stacks to find the one that point should be added to, if any
    // (in none it returns null)
    function findStack(stacks, svg, point, max) {
        var dist = max, selected_stack = null;
        stacks.forEach(function (stack) {
            var d = distance(svg, stack.point(), point);
            if (d < dist) {
                dist = d;
                selected_stack = stack;
            }
        });
        if (!selected_stack) {
            // XXX DEBUG
            console.log("no stack ...");
        }
        return selected_stack;
    }
    d3.pathstack = function (point) {
        return new PathStack(point);
    }
    d3.pathstack.findStack = findStack;
    d3.pathstack.distance  = distance;
    return d3.pathstack;
});
