/* handles the UI for loading CSV files and returns CSV based route
 * objects */

define(["trademapper", "d3"], function(tm, d3) {

    var callback = null;        // the callback that will be called
                                // when our work is done.

    var csv;                    // this will stay static so that you
                                // can re-filter etc without having to
                                // reload the data

    var hasHeaders = true;

    /* this defines how the csv data should be converted into a
     * route. It will allow the data to be modified in a number of
     * ways such as filtering out unwanted cols and rows, and
     * assigning the standard route roles to a column. It consists of
     * an array with one entry for each column in the CSV array. Each
     * array will be assigned an object which has two properies:
     *
     * action: what should be done with this column
     *
     * value: a value that will be treated differently depending on the
     *       action
     *
     * action can be one of:
     *
     *  + "filter" -> only select the row if its value for this column
     *  matches the value
     *
     *  + "skip"   -> ignore the column entirely (the default)
     *
     *  + "routeRole" -> assign the route role (e.g. source, dest etc)
     *  specified by value
     */

    var export_rules = null;
    var actions = [ "filter", "skip", "routeRole" ];
    var routeRoles = [ "source", "dest", "weight" ];

    function previewCSV(s) {
        // is the first row text headers? assume no to start off with
        var headers;
        var number_to_preview = 4;

        if(s) {                 // optional arg
            csv = d3.csv.parseRows(s);
            // initialise the default export_rules
            export_rules = [];
            for (i = 0; i < csv[0].length; i++) {
                export_rules[i] = { action: "skip", value: null };
            }
        }

        var thead = d3.select("#csv_input_preview_table thead");
        var tbody = d3.select("#csv_input_preview_table tbody")

        d3.select("#csv_input_preview_header_chk").attr("checked", hasHeaders);

        function addRows(parent, data, html) {
            if(arguments.length < 3) {
                html = false;
            }
            var rows = parent.selectAll("tr")
                .data(data);

            rows.enter().append("tr");
            rows.exit().remove();

            var cells = rows.selectAll("td")
                .data(function(d) { return d; });
            cells.enter().append("td");
            cells.exit().remove();
            if(html) {
                cells.html(function(d) { return d; })
            } else {
                cells.text(function(d) { return d; });
            }
            return rows;
        }

        // choose and then create an appropriate value entry UI
        // component based on the provided action
        function addValueUI(parent, action, index) {
            var sel, opt;

            switch (action) {
            case "skip":
                // we don't need to do anything for this one
                parent.selectAll(".value_ui").remove();
                break;
            case "filter":
                // temporary debug action
                parent.append("div")
                    .text("filter - not implemented").attr("class", "value_ui");
                break;
            case "routeRole":
                // create a selection that lists the available route roles
                sel = parent
                    .append("select")
                    .attr("class", "value_ui")
                    .on("click", function () {
                        var chosen = parseInt(this.selectedIndex);

                        // index 0 is empty
                        export_rules[index].value = (chosen == 0) ?
                            null : routeRoles[chosen - 1];
                    });
                opt = sel.selectAll("option").data([""].concat(routeRoles));
                opt.enter().append("option");
                opt.text(String);
                break;
            }
        }

        function addExportRules(parent) {
            var row = parent.append("tr");
            var cells = row.selectAll("td").data(export_rules);
            var new_cells = cells.enter()
                .append("td")
                .classed("exportRulesUI", true)

            new_cells.append("select"); // one for each td

            cells.exit().remove();

            var sel = cells
                .select("select")
                .on("change", function(d, i) {
                    export_rules[i].action =
                        actions[parseInt(this.selectedIndex)];

                    // remove any value ui for this column
                    var parent = d3.select(this.parentNode);
                    parent.selectAll(".value_ui").remove();

                    // and add a new one -- this could take a number
                    // of different forms, depending on the action.
                    addValueUI(parent, export_rules[i].action, i);
                });

            var opt = sel.selectAll("option").data(function(d, i) {
                var ret = [];
                // add each possible action to the dataset, but for
                // the one that is currently selected, add the
                // selected as true
                for (var i = 0; i < actions.length; i++) {
                    ret.push({
                        name: actions[i],
                        selected: export_rules[i].action == actions[i]
                    });
                }
                return ret;
            });
            opt.enter().append("option");
            opt.exit().remove();

            opt.text(function (d) { return d.name; })
                .attr("selected", function (d) {
                    return d.selected ? "yes" : null;
                });

            return row;
        }

        // parse the CSV and update the preview with the current
        // settings
        function update() {
            var tbl = d3.select("#csv_input_preview_table");

            if(hasHeaders) {
                headers = csv[0];
            } else {
                headers = [];
            }

            addRows(thead, [headers]);

            /* now create the ui components from the the
             * export_rules */
            addExportRules(thead);

            /* add the data rows of the preview */
            var startRow = hasHeaders ? 1 : 0,
                    endRow = startRow + number_to_preview;
            addRows(tbody, csv.slice(startRow,endRow));

            // display the preview
            d3.select("#csv_input_preview").style("display", "block");
        }

        // UI action events
        d3.select("#csv_input_preview_header_chk")
            .on("change", function() {
                hasHeaders = this.checked;
                update();
            })

        // XXX DEBUG
        update();
    }

    // pass in the file object, e.g. as selected by the user, and load
    // it as text
    function readCSVFile(f) {
        var reader = new FileReader();
        var progress = d3.select("#csv_input_progress");
        var progress_txt = "Loading ... ";

        reader.onloadstart = function () {
            progress.text(progress_txt);
        }

        reader.onprogress = function () {
            progress.text(progress_txt += " . ");
        }

        reader.onloadend = function () {
            progress.text("");
        }

        reader.onerror = function () {
            d3.select("#csv_input.ui_float").style("display", "none");
            callback.call(reader, null);
        }

        reader.onload = function () {
            previewCSV(reader.result);
        }

        reader.readAsText(f);
    }

    // returns the current csv data exported using the current ruleset
    // as an array of routes
    function export_routes() {

        var route, routes = [], row, col, validRow, role;

        // go through each row of the table and apply the rules to
        // create the routes

        for(row = (hasHeaders) ? 1 : 0; row < csv.length; row++) {
            route = new tm.Route();
            validRow = true;
            for(col = 0; col < csv[row].length; col++) {
                // check the export rule for this column and act
                // accordingly
                switch (export_rules[col].action) {
                case "skip":
                    /* no action required */
                    break;
                case "filter":
                    // does this row match the criteria?
                    if(csv[row][col] != export_rules[col].value) {
                        validRow = false;
                    }
                    break;
                case "routeRole":
                    route[export_rules[col].value] = csv[row][col];
                    break;
                }
            }

            for(role = 0; role < routeRoles.length; role++) {
                if (!(routeRoles[role] in route)) {
                    console.log("route doesn't have required key " + routeRoles[role]);
                    validRow = false;
                }
            }

            var i = 0;
            if(validRow) {
                routes.push(route);
            }
        }

        return routes;

    }

    return tm.csv = {

        // ask the user to upload a file, parse it, and then call the
        // callback with the route obj, or null if there was a problem
        loadRouteCSV: function (cb) {
            callback = cb;
            // load the user interface
            var ui = d3.select("#csv_input.ui_float");
            ui.style("top", "20px")
                .style("display", "block");

            // and setup the events
            d3.select("#csv_input_close")
                .on("click", function () {
                    ui.style("display", "none");
                });

            d3.select("#csv_input_file")
                .on("change", function () {
                    var files = this.files;
                    if(files.length < 1) {
                        alert("Please choose a file");
                    } else {
                        // only one file at a time for now
                        readCSVFile(files[0]);
                    }
                });

            d3.select("#csv_input_mapit")
                .on("click", function () {
                    if(!csv || csv.length < 1) {
                        alert("Nothing to map");
                        return;
                    }
                    ui.style("opacity", "0.5");
                    var routes = export_routes();
                    ui.style("opacity", null);
                    ui.style("display", "none");
                    cb.call(null, routes);
                });
        }

    };
});
