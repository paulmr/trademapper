/* contains aliases that map alternative country names to IDs. Note
 * that these are normalised to all captial letters and no non-alpha
 * when searching */

define([], function(tm) {
    var $ = {};
    $.aliases = [
        // some hard-coded built-in aliases
        { alias: "UNITEDSTATESOFAMERICA", id: "US" },
        { alias: "CONGOTHEDEMOCRATICREPUBLICOFTHE", id: "CD" }
    ];

    function norm(s) {
        return s.toUpperCase().replace(RegExp("[^A-Z]", "g"), "");
    }

    $.match = function(country) {
        var c = norm(country);
        var matches = this.aliases.filter(function(f) {
            return f.alias == c;
        });
        if(matches.length > 0) {
            return matches[0].id; // only first match used
        } else {
            return null;
        }
    };

    $.add = function(alias, id) {
        // make sure it isn't already there
        if(!this.match(alias)) {
            this.aliases.push({ alias: norm(alias), id: id});
        }
        return this;
    }

    return $;
});
