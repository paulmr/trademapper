define(["d3", "worldmap", "trademapper.alias", "pathstack"], function(d3, countries_data, alias_data) {

// the stack points
var stacks = [];

// utility functions here

function point(p) {
    return { x: p[0], y: p[1] }
}

function edit_country(parent, data, cb) {
    var sel, opt;
    
    sel = d3.select(parent)
        .text(null).selectAll("select").data([].concat(data));
    sel.enter().append("select");

    opt = sel.selectAll("option").data(countries_data.features.map(function (x) {
        return { name: x.properties.name, id: x.id };
    }).sort(function (a, b) {
        return d3.ascending(a.name, b.name);
    }));

    opt.enter().append("option");
    opt.attr("value", function(d) { return d.name })
        .text(function (d) { return d.name; });

    sel.on("change", function (d) {
        var oldname = d.data[d.key], newname = this.value;
        if(confirm("Replace " + oldname + " with " + newname + "?")) {
            $.replace_country(oldname, newname);
        }
        d3.select(this).remove();
        cb(d.data[d.key]);
    });
}

function edit_number(parent, data, cb) {
    var i = d3.select(parent)
        .text(null)
        .selectAll("input").data([].concat(data));
    i.enter().append("input");
    i.attr("type", "text")
        .attr("value", function (d) { return d.data[d.key] })
        .attr("size", function (d) { return  String(d.data[d.key].length + 2) })
        .on("change", function (d) {
            var n = parseInt(this.value);
            d.data[d.key] = isNaN(n) ? 0 : n;
            d3.select(this).remove();
            cb(d.data[d.key]);
        });
}

// contructor for object that represents a route
function Route(src, dest, weight) {
    var dflt = "N/A";
    this.source = src ? src : dflt;
    this.dest   = dest ? dest : dflt;
    this.weight = weight ? weight : 0;
}

Route.prototype = {
    equal: function(route) {
        var res = route instanceof Route && (this.source == route.source &&
                   this.dest == route.dest);
        return res;
    },

    isValid: function () {
        var r = this;
        if(typeof(this.source) == "string" && typeof(this.dest == "string")) {
            if(this.source.length < 1 || $.findfeature(this.source) == null) {
                this.error = "Unkown source: " + this.source;
                return false;
            }
            if(this.dest.length < 1 || $.findfeature(this.dest) == null) {
                this.error = "Unknown dest: " + this.dest;
                return false;
            }
        } else {
            this.error = "Expecting source and dest";
            return false;
        }
        w = Number(this.weight);
        if(isNaN(w) || w < 0) {
            this.error = "Weight (" + this.weight + ") should be a number greater than 0";
            return false;
        }
        return true;
    },
};

function highlightData(high_d, on) {
    if(on) {
        d3.selectAll("g.route")
            .classed("lowlight", function(d) { return !d.equal(high_d); });
        var row_index = false;
        var rows = d3.selectAll("tr.route_tr")
            .each(function(d, i) {
                d3.select(this).classed("highlight", d === high_d);
                row_index = i;
            });
    } else {
        d3.selectAll("g.route.lowlight").classed("lowlight", false);
        d3.selectAll("tr.route_tr.highlight").classed("highlight", false);
    }
};

var $ = {
    scale: 20,
    cradius: 1,
    linewidth: {min: 0.5, max: 0.7},
    origin: [50, 50],
    routes: [],
    gradientStopData: (function (routeCol) {
        return [{ offset: 0, col: routeCol.start, cl: "stop_first"},
                { offset: 1, col: routeCol.end,   cl: "stop_last"}];
    })({ start: "blue", end: "red" }),
    gradientStopAttr: {
        "class" :     function (d) { return d.cl; },
        "offset":     function (d) { return d.offset; },
        "stop-color": function (d) { return d.col; }
    },
    nest: d3.nest()
        .key(function (d) { return d.source + "," + d.dest; })
        .rollup(function (d) {
            return new Route(d[0].source, d[0].dest,
                             d3.sum(d, function (d) { return d.weight; }));
        }),

    db: countries_data,
    /* elm should be an SVG node that we can write to */
    init: function(elm) {
        /* draw the underlying map from the geojson data as a mercator projection */
        $.proj = d3.geo.mercator()
                .scale($.scale)
                .translate($.origin);
        $.pathmaker = d3.geo.path().projection($.proj);
        $.svg = d3.select(elm);
        $.svg.append("path")
            .attr("class", "map_image")
            .attr("d", $.pathmaker(countries_data))

        // initial update
        $.update();

    },
    Route: Route,
    draw_routes: function(routes) {
        var p = $.proj;

        /* this will generate a 'diagonal' curve from a
         * (already projected) route object */

        var path = d3.svg.line()
            .interpolate("monotone")
            .x(function (d) { return d[0] })
            .y(function (d) { return d[1] });

        // empty the stack points so we can restart the calculation
        stacks.forEach(function (stack) { stack.empty(); });
        $.updateStackCircles = function () {
            var stack_circles = $.svg.selectAll("circle.stackpoint").data(stacks);

            // first draw the circles which mark out the stack points, for illustration
            // purposes
            stack_circles.enter()
                .append("circle")
                .attr("class", "stackpoint");

            stack_circles
                .attr("cx", function (d) { return d.point().x; })
                .attr("cy", function (d) { return d.point().y; })
                .attr("r",  1)
                .property("stackpoint", function (d) { return d; })
                .each(function (d) {
                    d.circle = this;
                });
        }


        /* create a 'g' container for each route */

        // just use the valid routes
        var routes_g = $.svg.selectAll("g.route")
            .data(routes.filter(function (r) {
                return (r.isValid() && r.weight > 0);
            }));

        /* remove extras if there is now more circles than
         * data (e.g some data has been removed since we last
         * drew */
        routes_g.exit().remove();

        var e = routes_g.enter().append("g").attr("class", "route");
        /* add the route visual elements */

        e.append("path").attr("class", "route_line")

        /* the routes might be specified as lat/lng
         * co-ordinates, or they might be the names of
         * countries we need to lookup up in the db. So here
         * we convert them all to projected cartesian
         * co-ordinates */
        routes_g.each(function (d) { $.getroute_coords(d); })
            .attr("title", function (d) {
                return d.source + " ⇒ " + d.dest + "; weight: " + d.weight;
            });

        /* now set the correct data for the circles in the update selection
         * [select() does data propoagation] */
        routes_g.select("circle.route_source").datum(function(d) {
            return {loc: d.coords[0], weight: d.weight};
        });
        routes_g.select("circle.route_dest").datum(function(d) {
            return {loc: d.coords[1], weight: d.weight}
        });

        /* create a scale that will convert transform the list
         * of routes in to a radius for our circle. */
        var r_scale = d3.scale.linear()
            .domain(d3.extent(routes.map(function(r) {
                return parseInt(r.weight);
            }).filter(function (weight) {
                return weight > 0;
            })))
            .range([$.linewidth.min, $.linewidth.max]);

        /* and now draw the circle according to the data... */
        routes_g.selectAll("circle")
            .attr("cx", function(d) { return d.loc[0] })
            .attr("cy", function(d) { return d.loc[1] })
            .attr("r", function(d)  { return 0.75 });

        // create the gradient which will start and end at the actual
        // co-ordinates of our line (and therefore will work
        // regardless of our direction etc)
        var stop = routes_g.select(".route_gradient")
            .attr({
                "id": function (d, i) { return "route_gradient" + i; },
                "x1": function (d) { return d.coords[0][0] },
                "y1": function (d) { return d.coords[0][1] },
                "x2": function (d) { return d.coords[1][0] },
                "y2": function (d) { return d.coords[1][1] }
            }).selectAll("stop").data($.gradientStopData);
        stop.enter().append("stop");
        stop.attr($.gradientStopAttr);

        /* ... followed by the lines (use the diag-generator
         * created above to convert the data into the svg path) */
        routes_g.select("path")
            .attr("marker-end", "url(#mark_arrow)")

        // the DOM seems to something behind the scense that messes
        // with quotes when I use the normal method of setting the
        // url() style here which breaks the XML serialization.
            .each(function (d, i) {
                this.setAttribute("style",
                               "stroke: lightgreen; " +
                                //"stroke: url(#route_gradient" + i + ") lightblue; " +
                                "stroke-width: " + r_scale(d.weight) + "px");
            })
            .each(function (d, i) {
                console.log(d);
                var selected_stack =
                    // stack in the middle of the line ...
                    d3.pathstack
                        .findStack(stacks, $.svg,
                            point(d.coords[0]),
                            d3.pathstack.distance($.svg,
                                point(d.coords[0]),
                                point(d.coords[1])));
                if (selected_stack != null) {
                    selected_stack.add(d, 0, r_scale(d.weight));
                }
            });
            stacks.forEach(function (s) { s.dostack(); });
            routes_g.selectAll("path")
                .datum(function (d) { return d.coords; })
                .attr("d", path);
            
        // initialise the hover action to highlight the item that the
        // cursor is over
        routes_g
            .on("mouseover", function (d) { highlightData(d, true); })
            .on("mouseout", function (d) { highlightData(d, false); });
    },

    draw_key: function() {
        var stop = d3.select("#mapkey_gradient")
            .selectAll("stop")
            .data($.gradientStopData);
        stop.enter().append("stop");
        stop.attr("class", function (d) { return d.cl; })
            .attr("offset", function (d) { return d.offset; })
            .attr("stop-color", function (d) { return d.col; });
    },

    list_routes: function(routes) {

        function edit_route(d) {
            // remove the click event while we are editing it
            d3.select(this).on("click", null);
            // choose the correct dialog */
            switch (d.key) {
            case "source":
            case "dest":
                // fall through
                edit_country(this, d, $.update);
                break;
            case "weight":
                // I am disabling this for now
//                edit_number(this, d, $.update);
                break;
            }
        }

        d3.select("#route_count").text("(count: " + routes.length + ")");

        rows = d3.select("table#route_list tbody")
            .selectAll("tr.route_tr")
            .data(routes);
        var new_rows = rows.enter()
            .append("tr").attr("class", "route_tr")

        /* delete any extras if we now have more rows than data */
        rows.exit().remove();

        rows.classed("invalid", function(d) { return !d.isValid(); })
            .attr("title", function(d) { return d.isValid() ? null : d.error; })
            .property("route", function(d) { return d; });
      /* convert the data from an object to a flat array */
        cells = rows.selectAll("td.data")
            .data(function(d) {
                return ["source", "dest", "weight" ].map(function (x) {
                    return { key: x, data: d };
                })
            });
        // insert before the delete button
        cells.enter().append("td").attr("class", "data");
        cells.text(function (d) { return d.data[d.key]; }).
            on("click", edit_route);

        /* add click event for the special delete cells */
        new_rows.append("td").attr("class", "delete").text("[delete]");
        rows.select("td.delete").on("click", function(d) {
            $.delete_route(d);
            $.update();
        });
        rows.on("mouseover", function(d) { highlightData(d, true) });
        rows.on("mouseout",  function(d) { highlightData(d, false) });

        d3.select("table#route_list")
            .selectAll("th").on("click", function (d) {
                var e = d3.select(this);
                rows.sort(function (a, b) {
                    return d3.ascending(a[e.text().toLowerCase()],
                                        b[e.text().toLowerCase()]);
                }).order();
            });
    },

    update: function() {
        // nest the routes
        var nested_routes = $.nest.map($.routes, d3.map).values();
        $.draw_routes(nested_routes);
        $.list_routes(nested_routes);
        $.updateStackCircles();
        $.draw_key();
    },

    /* a route consists of an object with a source, and a destination.
     * These can either be strings, in which case they are country names
     * and will be looked up in the geojson database. Otherwise they can be
     * an integer array which is lat/lng of the point */

    getroute_coords: function(route) {
        var f;
        var coords = []; 
        if(typeof(route.source) == "string") {
            f = $.findfeature(route.source, $.db);
            if(f == null) { alert("not found: " + route.source); return; }
            coords[0] = $.pathmaker.centroid(f);
            f = $.findfeature(route.dest, $.db);
            if(f == null) { alert("not found: " + route.dest); return; }
            coords[1] = $.pathmaker.centroid(f);
        } else {
            coords[0] = $.proj(route.source);
            coords[1] = $.proj(route.dest);
        }
        route.coords = coords;

        return route;
    },
    /* searches for the feature in the provided with db with the specified
     * name and returns it (or null if not found) */
    findfeature: function(name) {
        var db = $.db, alias = alias_data.match(name);

        // first check the aliases
        if(alias) {
            name = alias;
        }
        for(i = 0; i < db.features.length; i++) {
            if(db.features[i].properties.name.toLowerCase() ==
               name.toLowerCase() || db.features[i].id == name) {
                return db.features[i];
            }
        }
        return null;
    },
    /* add route - take the data from the form, and push it onto
     * the array, then trigger a display update */
    add_route: function() {
        var route = new Route(document.getElementById("frm_add_source").value,
                              document.getElementById("frm_add_dest").value,
                              Number(document.getElementById("frm_add_weight").value));

        $.routes.push(route);
        $.update();
    },

    /* removes the route which has the provided src and dest */
    delete_route: function(r) {
        $.routes = $.routes.filter(function(cmp) {
            return !r.equal(cmp);
        });
    },

    // for all instances of oldname in the route list (either source or
    // dest) replace it with new name)
    replace_country: function(oldname, newname) {
        $.routes.forEach(function(r) {
            if(r.source == oldname) {
                r.source = newname;
            }
            if(r.dest == oldname) {
                r.dest = newname;
            }
        });
        return $.routes;
    },

    /* returns true if r is a valid route, else false */
    valid_route: function(r) {
        return r.valid;
    },

    /* mostly for debugging, demo purposes, this will generate n
     * amount of random routes using a normal distribution of
     * weights, and random selections from the db */
    random_routes: function(n) {
        var routes = [], normal = d3.random.normal(1000);
        var cnames = $.db.features.map(function(x) { return x.properties.name; });
        d3.shuffle(cnames);
        for(i = 0; i < n; i++) {
            routes.push(new Route(cnames[i], cnames[cnames.length - i - 1],
                                  normal()));
        }
        return routes;
    },

    add_stack: function (point, width) {
        stacks.push(d3.pathstack(point).width(width));
        return this;
    },
    del_stack: function (sp) {
        stacks.splice(stacks.indexOf(sp), 1);
        return this;
    }
};

return $;
});

// XXX DEBUG
//trademapper.routes = trademapper.random_routes(10);
